# Тестовое задание

- BuildingSystem
	- "BuildingActors" - наследники BuildingWorldActor
	- "BuildingObjects" - классы UObject с логикой зданий
	- "UI" - виджет для отображения ресурса на здании 
	- BaseBuildingObject - базовый класс для BuildingObjects, с функционалом ресурса здания
	- BuildingComponent - компонент для игрока с функцией Build для строительства
	- BuildingSubsystem - сабсистема содержит объекты зданий BuildingObject которые находятся в мирах
	- BuildingWorldActor - класс для визуализации в 3д, без функциональной логики
    - BuildingDataAsset - класс содержащий данные нужные для зданий
- WorldSystem
	- WorldSubsystem - сабсистема в которой хранится вся информация о постройках в мирах и функционал для загрузки миров
#pragma once

#include "CoreMinimal.h"
#include "BaseBuildingObject.generated.h"

class UBuildingSubsystem;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnResourceUpdatedSignature, int32, Resource);

UCLASS(Abstract)
class TESTTASK_API UBaseBuildingObject : public UObject
{
	GENERATED_BODY()
	
	friend UBuildingSubsystem;

private:
	int32 Id;

protected:
	TSharedPtr<int32> Resource;

public:
	UPROPERTY(BlueprintAssignable)
	FOnResourceUpdatedSignature OnResourceUpdatedDelegate;
	
	UBaseBuildingObject();
	
	int32 GetId() const {return Id;};
	virtual int32 GetResource() { return *Resource;};
	virtual void SetResourceValue(int32 Value);
	void IncrementResource(int32 value = 1);
	void DecrementResource(int32 value = 1);
	
	virtual void Initialize() {};
};

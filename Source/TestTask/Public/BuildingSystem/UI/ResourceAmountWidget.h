#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "ResourceAmountWidget.generated.h"

class UBaseBuildingObject;
class UTextBlock;

/**
 * 
 */
UCLASS()
class TESTTASK_API UResourceAmountWidget : public UUserWidget
{
	GENERATED_BODY()

protected:
	UPROPERTY(meta = (BindWidget))
	UTextBlock* ResourceAmountTextBlock;

	UFUNCTION()
	void UpdateResource(int32 Amount);

public:
	UFUNCTION()
	void Init(UBaseBuildingObject* BuildingObject);
};

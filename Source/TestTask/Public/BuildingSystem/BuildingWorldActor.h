#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"

#include "BuildingWorldActor.generated.h"

class UWidgetComponent;
class UBuildingComponent;
class UBaseBuildingObject;

UCLASS(Abstract)
class TESTTASK_API ABuildingWorldActor : public AActor
{
	GENERATED_BODY()

	friend UBuildingComponent;

protected:
	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly)
	USceneComponent* ActorRoot;
	
	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly)
	UStaticMeshComponent* Mesh;

	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly)
	UWidgetComponent* ResourceWidget;
	
	UPROPERTY()
	UBaseBuildingObject* BuildingObject;
	
public:
	ABuildingWorldActor(const FObjectInitializer& ObjectInitializer);
	UBaseBuildingObject* GetBuildingObject() const {return BuildingObject;};

	virtual bool Initialize();
};

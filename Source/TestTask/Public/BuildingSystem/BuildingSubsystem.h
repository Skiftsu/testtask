#pragma once

#include "CoreMinimal.h"
#include "BaseBuildingObject.h"
#include "Subsystems/GameInstanceSubsystem.h"
#include "BuildingSubsystem.generated.h"

class ABuildingWorldActor;

/**
 * 
 */
UCLASS()
class TESTTASK_API UBuildingSubsystem : public UGameInstanceSubsystem
{
	GENERATED_BODY()

private:
	int32 LastID = 0;
	
	UPROPERTY()
	TMap<int32, UBaseBuildingObject*> Buildings;

public:
	int32 AddBuilding(UBaseBuildingObject* Object, ABuildingWorldActor* WorldActor);
	UBaseBuildingObject* GetBuildingById(int32 Id);
};

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "BuildingDataAsset.generated.h"

class UBaseBuildingObject;
class ABuildingWorldActor;

constexpr auto BuildingAssetType = "Buildings";

/**
 * 
 */
UCLASS(NotBlueprintable, NotPlaceable)
class TESTTASK_API UBuildingDataAsset : public UPrimaryDataAsset
{
	GENERATED_BODY()

public:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	TSoftClassPtr<UBaseBuildingObject> BuildingObjectClass;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	TSoftClassPtr<ABuildingWorldActor> BuildingWorldActorClass;

	FORCEINLINE bool VariableValidation() const
	{
		return BuildingObjectClass != nullptr && BuildingWorldActorClass !=nullptr;
	};
};

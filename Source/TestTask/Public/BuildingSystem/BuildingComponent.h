#pragma once

#include "CoreMinimal.h"
#include "BuildingWorldActor.h"
#include "Components/ActorComponent.h"
#include "BuildingComponent.generated.h"


class UBuildingDataAsset;

UCLASS(ClassGroup="BuildingSystem", meta=(BlueprintSpawnableComponent))
class TESTTASK_API UBuildingComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	UPROPERTY(EditDefaultsOnly, Category = "Settings")
	float DistanceForBuilding = 500.f;

	UPROPERTY(EditDefaultsOnly, Category = "Settings")
	bool bShowDebugTrace;

	UFUNCTION(BlueprintCallable, Category="BuildingSystem")
	void Build(UBuildingDataAsset* BuildingDataAsset);

	/* The Initialize function must be called */
	static ABuildingWorldActor* SpawnBuildingWorldActor(UWorld* WorldContext, TSubclassOf<ABuildingWorldActor> BuildingActorClass, FTransform ActorTransform, UBaseBuildingObject* BuildingObject);

private:
	bool Trace(FVector& Location);
};

#pragma once

#include "CoreMinimal.h"
#include "BuildingSystem/BuildingWorldActor.h"
#include "TransmitterWorldActor.generated.h"

class UArrowComponent;

UCLASS(Abstract)
class TESTTASK_API ATransmitterWorldActor : public ABuildingWorldActor
{
	GENERATED_BODY()

protected:
	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly)
	UArrowComponent* Arrow;
	
public:
	ATransmitterWorldActor(const FObjectInitializer& ObjectInitializer);

protected:
	virtual bool Initialize() override;
};
#pragma once

#include "CoreMinimal.h"
#include "BuildingSystem/BaseBuildingObject.h"
#include "ResourceSourceBuildingObject.generated.h"

/**
 * 
 */
UCLASS()
class TESTTASK_API UResourceSourceBuildingObject : public UBaseBuildingObject
{
	GENERATED_BODY()

protected:
	UPROPERTY(EditInstanceOnly)
	int32 StartingResourceAmount=20;

public:
	UResourceSourceBuildingObject();
};

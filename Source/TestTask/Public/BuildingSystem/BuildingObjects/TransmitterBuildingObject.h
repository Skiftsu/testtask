#pragma once

#include "CoreMinimal.h"
#include "BuildingSystem/BaseBuildingObject.h"
#include "TransmitterBuildingObject.generated.h"



/**
 * 
 */
UCLASS()
class TESTTASK_API UTransmitterBuildingObject : public UBaseBuildingObject
{
	GENERATED_BODY()

protected:
	int32 TargetBuildingId=0;
	bool isTargetSet=false;
	UPROPERTY(EditInstanceOnly)
	float CooldownOnResourceTransfer = 1.f;
	
	void StartTransferResources();
	bool FindAndSetTarget();
	virtual bool isBuildingAvailableForResourceTransfer(UBaseBuildingObject* BuildingObject);

public:
	int32 GetTargetBuildingId() const {return TargetBuildingId;};
	bool GetIsTargetSet() const {return isTargetSet;};
	virtual void Initialize() override;
};

#pragma once

#include "CoreMinimal.h"
#include "TransmitterBuildingObject.h"
#include "PortalBuildingObject.generated.h"

DECLARE_MULTICAST_DELEGATE(FOnShareResourceUpdatedSignature);

/**
 * 
 */
UCLASS()
class TESTTASK_API UPortalBuildingObject : public UTransmitterBuildingObject
{
	GENERATED_BODY()

private:
	FOnShareResourceUpdatedSignature* OnShareResourceUpdatedDelegate;
	
	static TSharedPtr<int32> GetShareResource(int32 Value = 0);
	static FOnShareResourceUpdatedSignature* GetShareResourceUpdatedDelegate();

protected:
	virtual bool isBuildingAvailableForResourceTransfer(UBaseBuildingObject* BuildingObject) override;
	void ShareResourceUpdated();

public:
	UPortalBuildingObject();
	virtual void SetResourceValue(int32 Value) override;
};

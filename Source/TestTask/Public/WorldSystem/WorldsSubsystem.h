#pragma once

#include "CoreMinimal.h"
#include "Subsystems/GameInstanceSubsystem.h"
#include "WorldsSubsystem.generated.h"

class ABuildingWorldActor;


USTRUCT()
struct FBuildingInWorld
{
	GENERATED_BODY()

	/* Building Object Id */
	int32 Id;
	int32 WorldId;
	FTransform BuildingWorldTransform;
	UPROPERTY()
	TSubclassOf<ABuildingWorldActor> BuildingWorldClass;
};

class ABuildingWorldActor;
class UBaseBuildingObject;

/**
 * 
 */
UCLASS()
class TESTTASK_API UWorldsSubsystem : public UGameInstanceSubsystem
{
	GENERATED_BODY()
	
private:
	int32 CurrentWorldId = 0;

	UPROPERTY()
	/* Key - BuildingObjectId */
	TMap<int32, FBuildingInWorld> BuildingsInWorld;
	
	UPROPERTY()
	/* Key - BuildingObjectId */
	TMap<int32, ABuildingWorldActor*> BuildingActorsInCurrentWorld;

	TArray<FBuildingInWorld> GetAllBuildingsByWorldId(int32 WorldId);

	void PostWorldLoad(UWorld* World);

public:
	UWorldsSubsystem();
	
	UFUNCTION(BlueprintCallable)
	int32 GetCurrentWorldId() const {return CurrentWorldId;};
	
	UFUNCTION(BlueprintCallable)
	void LoadWorldById(int32 WorldId);

	UFUNCTION(BlueprintCallable)
	void NextWorld();

	UFUNCTION(BlueprintCallable)
	void PreviousWorld();
	
	void AddBuildingActor(ABuildingWorldActor* BuildingActor, UBaseBuildingObject* Object);
	TArray<ABuildingWorldActor*> GetBuildingActorsInCurrentWorld();
	ABuildingWorldActor* GetBuildingActorById(int32 Id);
};

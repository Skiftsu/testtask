#include "BuildingSystem/BuildingWorldActor.h"

#include "BuildingSystem/BaseBuildingObject.h"
#include "BuildingSystem/UI/ResourceAmountWidget.h"
#include "Components/WidgetComponent.h"

#include "MainLogChannels.h"

class UBuildingSubsystem;

ABuildingWorldActor::ABuildingWorldActor(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	ActorRoot = CreateDefaultSubobject<USceneComponent>(TEXT("ActorRoot"));
	SetRootComponent(ActorRoot);
	
	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
	Mesh->SetupAttachment(GetRootComponent());
	
	ResourceWidget = CreateDefaultSubobject<UWidgetComponent>(TEXT("ResourceWidget"));
	ResourceWidget->SetupAttachment(GetRootComponent());
	ResourceWidget->SetCollisionEnabled(ECollisionEnabled::NoCollision);
}

bool ABuildingWorldActor::Initialize()
{
	if(!BuildingObject)
	{
		UE_LOG(LogBuildingSystem, Error, TEXT("Trying to create a building yourself"));
		return false;
	}
	
	if(ResourceWidget->GetWidget())
		Cast<UResourceAmountWidget>(ResourceWidget->GetWidget())->Init(BuildingObject);

	return true;
}
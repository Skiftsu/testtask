#include "BuildingSystem/BuildingSubsystem.h"

#include "Kismet/GameplayStatics.h"
#include "WorldSystem/WorldsSubsystem.h"


int32 UBuildingSubsystem::AddBuilding(UBaseBuildingObject* Object, ABuildingWorldActor* WorldActor)
{
	LastID++;
	Buildings.Add(LastID, Object);
	Object->Id = LastID;
	
	auto WorldsSubsystem = UGameplayStatics::GetGameInstance(GetWorld())->GetSubsystem<UWorldsSubsystem>();
	WorldsSubsystem->AddBuildingActor(WorldActor, Object);
	
	Object->Initialize();
	
	return LastID;
}

UBaseBuildingObject* UBuildingSubsystem::GetBuildingById(int32 Id)
{
	auto element = Buildings.Find(Id);
	check(element);
	return *element;
}
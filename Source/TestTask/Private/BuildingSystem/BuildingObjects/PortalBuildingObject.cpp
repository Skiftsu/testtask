#include "BuildingSystem/BuildingObjects/PortalBuildingObject.h"

TSharedPtr<int32> UPortalBuildingObject::GetShareResource(int32 Value)
{
	static TSharedPtr<int32> Instance;
	if(!Instance.IsValid())
	{
		Instance = MakeShared<int32>(Value);
	}
	
	return Instance;
}

FOnShareResourceUpdatedSignature* UPortalBuildingObject::GetShareResourceUpdatedDelegate()
{
	static FOnShareResourceUpdatedSignature* Instance;
	if(Instance) return Instance;
	Instance = new FOnShareResourceUpdatedSignature();
	return Instance;
}

bool UPortalBuildingObject::isBuildingAvailableForResourceTransfer(UBaseBuildingObject* BuildingObject)
{
	return BuildingObject->GetClass() != GetClass();
}

void UPortalBuildingObject::ShareResourceUpdated()
{
	OnResourceUpdatedDelegate.Broadcast(GetResource());
}

UPortalBuildingObject::UPortalBuildingObject()
{
	// Memory release
	Resource.Reset();
	Resource = GetShareResource();
	OnShareResourceUpdatedDelegate = GetShareResourceUpdatedDelegate();
	OnShareResourceUpdatedDelegate->AddUObject(this, &UPortalBuildingObject::ShareResourceUpdated);
}

void UPortalBuildingObject::SetResourceValue(int32 Value)
{
	Super::SetResourceValue(Value);
	OnShareResourceUpdatedDelegate->Broadcast();
}
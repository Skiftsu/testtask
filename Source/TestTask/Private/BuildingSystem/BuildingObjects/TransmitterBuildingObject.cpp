#include "BuildingSystem/BuildingObjects/TransmitterBuildingObject.h"

#include "BuildingSystem/BuildingSubsystem.h"
#include "BuildingSystem/BuildingWorldActor.h"
#include "Kismet/GameplayStatics.h"
#include "WorldSystem/WorldsSubsystem.h"


void UTransmitterBuildingObject::Initialize()
{
	Super::Initialize();

	isTargetSet = FindAndSetTarget();
	if(isTargetSet)
		StartTransferResources();
}

void UTransmitterBuildingObject::StartTransferResources()
{
	auto BuildingSubsystem = UGameplayStatics::GetGameInstance(GetWorld())->GetSubsystem<UBuildingSubsystem>();
	auto Target = BuildingSubsystem->GetBuildingById(TargetBuildingId);
	check(Target);
	
	if(Target->GetResource() == 0) return;

	FTimerManager* TimerManger = &GetWorld()->GetTimerManager();
	
	FTimerHandle TimerHandle;
	GetWorld()->GetTimerManager().ClearTimer(TimerHandle);
	auto TransferCallback = [this, Target, &TimerHandle, TimerManger]() {
		if(Target->GetResource() == 0)
		{
			TimerManger->ClearTimer(TimerHandle);
			return;
		}
		this->IncrementResource();
		Target->DecrementResource();
		if(Target->GetResource() == 0)
			TimerManger->ClearTimer(TimerHandle);
	};
	TimerManger->SetTimer(TimerHandle, TransferCallback, CooldownOnResourceTransfer, true);
}

bool UTransmitterBuildingObject::FindAndSetTarget()
{
	auto WorldsSubsystem = UGameplayStatics::GetGameInstance(GetWorld())->GetSubsystem<UWorldsSubsystem>();
	auto SelfActor = WorldsSubsystem->GetBuildingActorById(GetId());
	auto BuildingActors = WorldsSubsystem->GetBuildingActorsInCurrentWorld();

	
	FVector CurrentLocation = SelfActor->GetActorLocation();
	ABuildingWorldActor* NearestBuilding = nullptr;
	float NearestDistance = FLT_MAX;

	for (auto i : BuildingActors)
	{
		if(!isBuildingAvailableForResourceTransfer(i->GetBuildingObject())) continue;
		
		float Distance = FVector::Dist(CurrentLocation, i->GetActorLocation());
		if (Distance < NearestDistance)
		{
			NearestDistance = Distance;
			NearestBuilding = i;
		}
	}

	if(NearestBuilding)
	{
		TargetBuildingId = NearestBuilding->GetBuildingObject()->GetId();
		return true;
	}
	return false;
}

bool UTransmitterBuildingObject::isBuildingAvailableForResourceTransfer(UBaseBuildingObject* BuildingObject)
{
	return BuildingObject != this;
}
#include "BuildingSystem/BuildingComponent.h"

#include "DrawDebugHelpers.h"
#include "BuildingSystem/BaseBuildingObject.h"
#include "BuildingSystem/BuildingDataAsset.h"
#include "BuildingSystem/BuildingWorldActor.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "BuildingSystem/BuildingSubsystem.h"
#include "MainLogChannels.h"


void UBuildingComponent::Build(UBuildingDataAsset* BuildingDataAsset)
{
	if(!BuildingDataAsset)
	{
		UE_LOG(LogBuildingSystem, Error, TEXT("UBuildingComponent::Build | Data asset not specified"));
		return;
	} else if (!BuildingDataAsset->VariableValidation())
	{
		UE_LOG(LogBuildingSystem, Error, TEXT("UBuildingComponent::Build | Data Asset is not configured"));
		return;
	}
	
	FVector Location;
	if(!Trace(Location)) return;

	auto BuildingSubsystem = UGameplayStatics::GetGameInstance(GetWorld())->GetSubsystem<UBuildingSubsystem>();

	// Create Building Object
	auto BuildingObject = NewObject<UBaseBuildingObject>(BuildingSubsystem, BuildingDataAsset->BuildingObjectClass.Get());

	// Spawn Building World Actor
	auto RotationYaw = UKismetMathLibrary::FindLookAtRotation(GetOwner()->GetActorLocation(), Location).Yaw;
	const FRotator ActorRotation = FRotator(0.f, RotationYaw, 0.f);
	const FTransform ActorTransform = FTransform(ActorRotation, Location, FVector(1.f, 1.f, 1.f));
	auto BuildingActor = SpawnBuildingWorldActor(GetWorld(), BuildingDataAsset->BuildingWorldActorClass.LoadSynchronous(), ActorTransform, BuildingObject);
	
	// Add building in Subsystem
	BuildingSubsystem->AddBuilding(BuildingObject, BuildingActor);

	BuildingActor->Initialize();
}

ABuildingWorldActor* UBuildingComponent::SpawnBuildingWorldActor(UWorld* WorldContext, TSubclassOf<ABuildingWorldActor> BuildingActorClass, FTransform ActorTransform, UBaseBuildingObject* BuildingObject)
{
	auto BuildingActor = WorldContext->SpawnActorDeferred<ABuildingWorldActor>(BuildingActorClass, ActorTransform, nullptr, nullptr, ESpawnActorCollisionHandlingMethod::AlwaysSpawn);
	checkf(BuildingActor, TEXT("Actor was not created"));
	BuildingActor->BuildingObject = BuildingObject;
	BuildingActor->FinishSpawning(ActorTransform, true);
	return BuildingActor;
}

bool UBuildingComponent::Trace(FVector& Location)
{
	Location = FVector();
	
	auto PC = Cast<APlayerController>(Cast<APawn>(GetOwner())->GetController());
	check(PC);
	FVector ViewStart;
	FRotator ViewRot;
	PC->GetPlayerViewPoint(ViewStart, ViewRot);
	const FVector ViewDir = ViewRot.Vector();
	FVector ViewEnd = ViewStart + (ViewDir * DistanceForBuilding);

	FCollisionQueryParams Params = FCollisionQueryParams::DefaultQueryParam;
	Params.AddIgnoredActor(GetOwner());
	
	bool Result = false;
	FHitResult OutHitResult;
	if(GetWorld()->LineTraceSingleByChannel(OutHitResult, ViewStart, ViewEnd, ECollisionChannel::ECC_Visibility, Params))
	{
		if(OutHitResult.GetActor())
		{
			Location = OutHitResult.Location;
			Result = true;
		}
	}
	
	if (bShowDebugTrace)
	{
		auto World = GetWorld();
		FColor DebugColor = Result ? FColor::Green : FColor::Blue;
		float LifeTime = 10.f;
		if(Result)
		{
			DrawDebugLine(World, ViewStart, OutHitResult.Location, DebugColor, false, LifeTime);
			DrawDebugSphere(World, OutHitResult.Location, 5, 16, DebugColor, false, LifeTime);
		}
		else if (OutHitResult.bBlockingHit)
			DrawDebugLine(World, ViewStart, OutHitResult.Location, DebugColor, false, LifeTime);
		else
			DrawDebugLine(World, ViewStart, ViewEnd, DebugColor, false, LifeTime);
	}
	
	return Result;
}


#include "BuildingSystem/BuildingActors/TransmitterWorldActor.h"

#include "BuildingSystem/BuildingObjects/TransmitterBuildingObject.h"
#include "Components/ArrowComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "WorldSystem/WorldsSubsystem.h"


ATransmitterWorldActor::ATransmitterWorldActor(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	Arrow = CreateDefaultSubobject<UArrowComponent>(TEXT("Arrow"));
	Arrow->SetupAttachment(GetRootComponent());
	Arrow->SetHiddenInGame(false);
}

bool ATransmitterWorldActor::Initialize()
{
	if(!Super::Initialize()) return false;

	auto TransmitterObject = Cast<UTransmitterBuildingObject>(BuildingObject);
	
	auto WorldsSubsystem = UGameplayStatics::GetGameInstance(GetWorld())->GetSubsystem<UWorldsSubsystem>();
	if(TransmitterObject->GetIsTargetSet())
	{
		auto TargetActor = WorldsSubsystem->GetBuildingActorById(TransmitterObject->GetTargetBuildingId());
		check(TargetActor);
		auto ArrowRotation = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), TargetActor->GetActorLocation());
		Arrow->SetWorldRotation(ArrowRotation);
	}
	else
		Arrow->SetVisibility(false);

	return true;
}

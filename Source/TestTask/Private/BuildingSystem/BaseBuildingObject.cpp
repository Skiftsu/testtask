#include "BuildingSystem/BaseBuildingObject.h"


UBaseBuildingObject::UBaseBuildingObject() : Super()
{
	Resource = MakeShared<int32>(0);
}

void UBaseBuildingObject::SetResourceValue(int32 Value)
{
	*Resource = Value;
	OnResourceUpdatedDelegate.Broadcast(*Resource);
}

void UBaseBuildingObject::IncrementResource(int32 value)
{
	SetResourceValue(*Resource + value);
}

void UBaseBuildingObject::DecrementResource(int32 value)
{
	check(*Resource != 0);
	SetResourceValue(*Resource - value);
}
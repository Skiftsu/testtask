#include "BuildingSystem/UI/ResourceAmountWidget.h"

#include "BuildingSystem/BaseBuildingObject.h"
#include "Components/TextBlock.h"


void UResourceAmountWidget::UpdateResource(int32 Amount)
{
	ResourceAmountTextBlock->SetText(FText::AsNumber(Amount));
}

void UResourceAmountWidget::Init(UBaseBuildingObject* BuildingObject)
{
	UpdateResource(BuildingObject->GetResource());
	BuildingObject->OnResourceUpdatedDelegate.AddDynamic(this, &UResourceAmountWidget::UpdateResource);
}
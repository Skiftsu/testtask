#include "WorldSystem/WorldsSubsystem.h"

#include "BuildingSystem/BuildingSubsystem.h"
#include "BuildingSystem/BaseBuildingObject.h"
#include "BuildingSystem/BuildingComponent.h"
#include "BuildingSystem/BuildingWorldActor.h"
#include "Kismet/GameplayStatics.h"

TArray<FBuildingInWorld> UWorldsSubsystem::GetAllBuildingsByWorldId(int32 WorldId)
{
	TArray<FBuildingInWorld> AllBuildingsArray;
	BuildingsInWorld.GenerateValueArray(AllBuildingsArray);

	TArray<FBuildingInWorld> BuildingsArray;
	for(auto i : AllBuildingsArray)
	{
		if(i.WorldId == WorldId)
			BuildingsArray.Add(i);
	}
	return BuildingsArray;
}

void UWorldsSubsystem::PostWorldLoad(UWorld* World)
{
	auto BuildingsToSpawn = GetAllBuildingsByWorldId(CurrentWorldId);

	auto BuildingSubsystem = UGameplayStatics::GetGameInstance(World)->GetSubsystem<UBuildingSubsystem>();
	for(auto i : BuildingsToSpawn)
	{
		auto BuildingObject = BuildingSubsystem->GetBuildingById(i.Id);
		auto BuildingActor = UBuildingComponent::SpawnBuildingWorldActor(World, i.BuildingWorldClass, i.BuildingWorldTransform, BuildingObject);
		BuildingActorsInCurrentWorld.Add(BuildingObject->GetId(), BuildingActor);
	}

	TArray<ABuildingWorldActor*> BuildingWorldActorsArray;
	BuildingActorsInCurrentWorld.GenerateValueArray(BuildingWorldActorsArray);
	for(auto i : BuildingWorldActorsArray)
	{
		i->Initialize();
	}
}

UWorldsSubsystem::UWorldsSubsystem() : Super()
{
	FCoreUObjectDelegates::PostLoadMapWithWorld.AddUObject(this, &UWorldsSubsystem::PostWorldLoad);
}

void UWorldsSubsystem::LoadWorldById(int32 WorldId)
{
	BuildingActorsInCurrentWorld.Empty();
	
	CurrentWorldId = WorldId;
	//TODO Maybe move "LevelName" to the config
	FName LevelName = "L_Main";
	UGameplayStatics::OpenLevel(this, LevelName, true);
}

void UWorldsSubsystem::NextWorld()
{
	LoadWorldById(CurrentWorldId+1);
}

void UWorldsSubsystem::PreviousWorld()
{
	if(CurrentWorldId==0) return;
	LoadWorldById(CurrentWorldId-1);
}

void UWorldsSubsystem::AddBuildingActor(ABuildingWorldActor* BuildingActor, UBaseBuildingObject* Object)
{
	BuildingActorsInCurrentWorld.Add(Object->GetId(),BuildingActor);
	
	auto BuildingInWorld = FBuildingInWorld{Object->GetId(), CurrentWorldId, BuildingActor->GetTransform(), BuildingActor->GetClass()};
	BuildingsInWorld.Add(Object->GetId(), BuildingInWorld);
}

TArray<ABuildingWorldActor*> UWorldsSubsystem::GetBuildingActorsInCurrentWorld()
{
	TArray<ABuildingWorldActor*> Result;
	BuildingActorsInCurrentWorld.GenerateValueArray(Result);
	return Result;
}

ABuildingWorldActor* UWorldsSubsystem::GetBuildingActorById(int32 Id)
{
	auto Result = BuildingActorsInCurrentWorld.Find(Id);
	if(Result)
		return *Result;
	return nullptr;
}